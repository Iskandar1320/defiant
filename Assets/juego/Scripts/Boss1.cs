using MoreMountains.CorgiEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss1 : MonoBehaviour
{
    [SerializeField] protected GameObject ObjetoHacha;
    [SerializeField] protected GameObject Plataforma;
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            Plataforma.gameObject.SetActive(false);
            Destroy(ObjetoHacha);
        }
    }
}