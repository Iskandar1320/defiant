using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
//using System.Collections.Specialized;
//using System.Security.Cryptography;
//using System.Threading;
using UnityEngine;

public class Boss3 : MonoBehaviour
{
    Vector2 Enemypos;
    public GameObject PlayerM;
    bool perseguir;
    public int vel;
    
    // Update is called once per frame
    void Update()
    {
        if(perseguir)
        {
            transform.position = Vector2.MoveTowards(transform.position, Enemypos, vel * Time.deltaTime);
        }

        if(Vector2.Distance(transform.position, Enemypos) > 12f)
        {
            perseguir = false;
        }
    }

    private void OntriggerStay2D(Collider2D collision)
    {
        if (collision.tag.Equals("Player"))
        {
            Enemypos = PlayerM.transform.position;
            perseguir = true;
        }
    }
}
